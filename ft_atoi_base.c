/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/26 12:29:54 by bbichero          #+#    #+#             */
/*   Updated: 2019/02/28 17:49:23 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void					ft_atoi_base(void *ptr, int base)
{
	unsigned long long	address;

	address = (unsigned long long)ptr;
	ft_putendl(ft_itoa_base(address, base));
}
