/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoabase.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/28 15:57:47 by bbichero          #+#    #+#             */
/*   Updated: 2019/03/05 17:05:49 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char				*ft_itoa_base(long long nbr, int base)
{
	char			*str;
	int				i;
	int				neg;

	i = PADDING_SIZE;
	neg = 0;
	if (nbr < 0)
	{
		if (base == 10)
			neg = 1;
		nbr *= -1;
	}
	str = (char *)malloc(sizeof(char) * PADDING_SIZE + 1);
	ft_bzero(str, PADDING_SIZE + 1);
	str[PADDING_SIZE + neg] = '\0';
	while (nbr && --i >= 0)
	{
		str[i + neg] = (nbr % base) + (nbr % base > 9 ? 'A' - 10 : '0');
		nbr /= base;
	}
	while (--i >= 0)
		str[i + neg] = '0';
	if (neg)
		str[0] = '-';
	return (str);
}
