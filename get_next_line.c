/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <bbichero@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/21 00:37:43 by bbichero          #+#    #+#             */
/*   Updated: 2017/07/25 11:43:13 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_read(int fd, char **tmp)
{
	int			ret;
	char		buf[BUF_SIZE + 1];

	if (*tmp != NULL && ft_strchr(*tmp, '\n'))
		return (1);
	ft_bzero(buf, sizeof(buf));
	while ((ret = read(fd, buf, BUF_SIZE)) > 0)
	{
		if (*tmp == NULL)
		{
			*tmp = ft_strnew(1);
			*tmp = "";
		}
		*tmp = ft_strjoin(*tmp, buf);
		if (*tmp == NULL)
			return (-1);
		if (ft_strchr(*tmp, '\n') != NULL)
			break ;
		ft_bzero(buf, sizeof(buf));
	}
	return (ret <= 0 ? ret : 1);
}

void		ft_str(char **line, char **tmp, int *ret)
{
	char		*ptr;

	if (*tmp != NULL && ft_strlen(*tmp) > 0)
	{
		*ret = 1;
		ptr = ft_strchr(*tmp, '\n');
		if (ptr == NULL)
		{
			*line = *tmp;
			*tmp = NULL;
		}
		else
		{
			*line = *tmp;
			*ptr = '\0';
			*tmp = ft_strdup(ptr + 1);
		}
	}
	return ;
}

int			get_next_line(const int fd, char **line)
{
	int			ret;
	static char	*current = NULL;

	if (line == NULL)
		return (-1);
	*line = NULL;
	ret = ft_read(fd, &current);
	if (ret < 0)
		return (-1);
	ft_str(line, &current, &ret);
	return (ret);
}
