/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <bbichero@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 15:44:13 by bbichero          #+#    #+#             */
/*   Updated: 2015/04/14 09:18:05 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int				ft_atoi(const char *str)
{
	unsigned int		nb;
	int					valeur;
	int					positive;

	nb = 0;
	valeur = 0;
	while (*str == ' ' || *str == '\n' || *str == '\v' || *str == '\r' ||
			*str == '\f' || *str == '\t')
		str++;
	positive = (*str == '-' ? -1 : 1);
	if (*str == '-' || *str == '+')
		str++;
	while (*str == '0')
		str++;
	while (ft_isdigit(*str) && *str != '\0')
	{
		nb = (int)(*str - '0');
		valeur = (valeur * 10) + nb;
		str++;
	}
	return (valeur * positive);
}
