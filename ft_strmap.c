/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <bbichero@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/10 11:02:01 by bbichero          #+#    #+#             */
/*   Updated: 2015/04/14 08:53:40 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strmap(char const *s, char (*f)(char))
{
	int			i;
	int			length;
	char		*str;

	i = 0;
	if (s == NULL)
		return (NULL);
	if (f == NULL)
		return (NULL);
	length = ft_strlen(s);
	str = ft_strnew(length);
	while (i < length)
	{
		str[i] = (*f)(s[i]);
		i++;
	}
	return (str);
}
