/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/28 17:40:57 by bbichero          #+#    #+#             */
/*   Updated: 2019/03/02 11:35:56 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char				*ft_itoa(long long nbr)
{
	char			*str;
	int				i;
	int				neg;

	i = 0;
	neg = 0;
	if (nbr < 0)
	{
		neg = 1;
		nbr *= -1;
	}
	while (ft_pow(10, i) - 1 < (unsigned long long)nbr)
		i++;
	str = (char *)malloc(sizeof(char) * i + 1);
	ft_bzero(str, i + 1);
	str[i + neg] = '\0';
	while (nbr && i-- >= 0)
	{
		str[i + neg] = (nbr % 10) + (nbr % 10 > 9 ? '1' : '0');
		nbr /= 10;
	}
	if (neg)
		str[0] = '-';
	return (str);
}
