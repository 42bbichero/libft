/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <bbichero@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/10 11:02:01 by bbichero          #+#    #+#             */
/*   Updated: 2015/04/14 08:48:42 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void			ft_bzero(void *s, size_t n)
{
	int			i;
	char		*q;

	i = 0;
	q = s;
	while (n != 0)
	{
		q[i] = 0;
		n--;
		i++;
	}
}
